import { Component, OnInit } from '@angular/core';
import { PedidosService } from 'src/app/pedidos.service';
import { Router } from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-pedido-form',
  templateUrl: './pedido-form.component.html',
  styleUrls: ['./pedido-form.component.css']
})
export class PedidoFormComponent {

  nomeCompleto = '';
  equipe = '';
  telefone = '';
  qtdMassaSolidaria = 0;
  entregaEspecial = 'Não';
  opcoes = [
    { id: 0, item: 'Pizza Marguerita', quantidades: [0, 0, 0]},
    { id: 1, item: 'Pizza Calabresa', quantidades: [0, 0, 0]},
    { id: 2, item: 'Pizza Frango com requeijão', quantidades: [0, 0, 0]},
    { id: 3, item: 'Nhoque à bolonhesa', quantidades: [0, 0, 0]},
    { id: 4, item: 'Rondelli quatro queijos', quantidades: [0, 0, 0]}
  ];

  equipes = [
    '22 de Outubro',
    'Americana',
    'Brinquedoteca HC',
    'Brinquedoteca PUC',
    'Brinquedoteca Sumaré',
    'Boldrini',
    'CAISM',
    'Centro Médico',
    'Comunicação',
    'Eventos',
    'Francisco Morato',
    'HC Quarta',
    'HC Sábado',
    'HC Segunda',
    'Indaiatuba',
    'Itinerante',
    'Jaguariúna',
    'Laranjal Paulista',
    'Mário Covas Quarta',
    'Mário Covas Sábado',
    'Mário Gatti',
    'Ouro Verde Quinta',
    'Ouro Verde Sábado',
    'Palhacinfônicos',
    'Paulínia',
    'Porto Feliz',
    'PUC Terça',
    'PUC Sábado',
    'Salto',
    'Santa Casa Mogi',
    'São Francisco',
    'Sede',
    'Sumaré Sábado',
    'Sumaré Segunda',
    'Tabajara',
    'Tatuí',
    'Tietê',
    'Vera Cruz',
    'Cerquilho',
    'Bazar'
  ].sort();

  constructor(private _pedidosService: PedidosService, private _router: Router, private _spinnerService: Ng4LoadingSpinnerService) {}

  registrarPedido() {
    this._spinnerService.show();
    this._pedidosService.addPedido({
      nome: this.nomeCompleto,
      equipe: this.equipe,
      telefone: this.telefone,
      entregaEspecial: this.entregaEspecial,
      qtdMassaSolidaria: this.qtdMassaSolidaria,
      opcoes: this.opcoes,
      incluidoEm: new Date().toISOString()
    }).then(res => {
      this._spinnerService.hide();
      this._router.navigate(['/pedido-recebido']);
    });
  }

  isValid() {
    return this.nomeCompleto !== '' && this.equipe !== '' && this.telefone !== ''
      && this.qtdMassaSolidaria >= 0 && this.quantidadesAreOK();
  }

  quantidadesAreOK() {
    let soma = 0;

    this.opcoes.forEach(element => {
      soma += element['quantidades'][0] + element['quantidades'][1] + element['quantidades'][2];
    });

    return soma > 0;
  }

  getSomaTotal() {
    let soma = 0;

    this.opcoes.forEach(element => {
      soma += element['quantidades'][0] + element['quantidades'][1] + element['quantidades'][2];
    });

    return soma + this.qtdMassaSolidaria;
  }

  getLabelEntrega(text) {
    switch (text) {
      case 'Sim-21-09':
        return 'Sim, meu pedido será retirado dia 21/09 (sexta-feira)';
      case 'Sim-22-09':
        return 'Sim, meu pedido será retirado dia 22/09 (sábado)';
      case 'Sim-23-09':
        return 'Sim, meu pedido será retirado dia 23/09 (domingo)';
      default:
        return 'Não haverá retirada ou entrega de pedido acima de 20 unidades';
    }
  }
}
