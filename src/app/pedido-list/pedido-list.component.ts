import { Component, OnInit } from '@angular/core';
import { PedidosService } from '../pedidos.service';
import * as XLSX from 'xlsx';
import { Md5 } from 'md5-typescript';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-pedido-list',
  templateUrl: './pedido-list.component.html',
  styleUrls: ['./pedido-list.component.css']
})
export class PedidoListComponent implements OnInit {


  pedidos: any[];

  constructor(private _pedidosService: PedidosService) {
    this._pedidosService.items.subscribe(res =>  {
      res.map(item => item.incluidoEm = new Date(item.incluidoEm));
      this.pedidos = res;

      this.pedidos = this.pedidos.sort((a, b) => (a.incluidoEm.getTime() - b.incluidoEm.getTime()));
    });
  }

  ngOnInit() {
    let pass = prompt('Digite a senha para acessar');

    while (Md5.init(pass) !== environment.pass) {
      pass = prompt('Digite a senha para acessar');
    }
  }

  removeItem(key, nome) {
    if (confirm('Deseja remover o pedido de ' + nome + '?')) {
      this._pedidosService.removePedido(key);
    }

  }

  exportExcel() {
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(document.getElementById('datatable'), {dateNF: 13 });

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, 'ExportForm_' + new Date().toISOString() + '.xlsx');
  }

  getSomaItem(indexItem, indexUnidade) {
    let soma = 0;

    this.pedidos.forEach(pedido => {
      soma += pedido.opcoes[indexItem]['quantidades'][indexUnidade];
    });

    return soma;
  }

  getSomaMassaSolidaria() {
    let soma = 0;

    this.pedidos.forEach(pedido => {
      if (pedido.qtdMassaSolidaria) {
        soma += pedido.qtdMassaSolidaria;
      }
    });

    return soma;
  }

}
