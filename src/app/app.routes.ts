
import { RouterModule, Routes } from '@angular/router';
import { PedidoListComponent } from './pedido-list/pedido-list.component';
import { PedidoFormComponent } from './pedido-form/pedido-form.component';
import { PedidoRecebidoComponent } from './pedido-recebido/pedido-recebido.component';

const APP_ROUTES: Routes = [
  {
    path: 'pedidos',
    component: PedidoListComponent
  },
  {
    path: 'pedido-recebido',
    component: PedidoRecebidoComponent
  },
  {
    path: '**',
    component: PedidoFormComponent
  }
];

export const routing = RouterModule.forRoot(APP_ROUTES);
