import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PedidosService {

  private pedidosCollection: AngularFirestoreCollection<any>;
  items: Observable<any[]>;

  constructor(private afs: AngularFirestore) {
    this.pedidosCollection = afs.collection<any>('pedidos');
    this.items = this.pedidosCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(action => {
          const data = action.payload.doc.data();
          const id = action.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }

  addPedido(item: any): Promise<any> {
    return this.pedidosCollection.add(item);
  }

  removePedido(key: string): Promise<any> {
    let itemDoc: AngularFirestoreDocument<any>;
    itemDoc = this.afs.doc<any>('pedidos/' + key);
    return itemDoc.delete();
  }
}
