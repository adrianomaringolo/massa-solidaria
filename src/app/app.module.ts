import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { PedidoFormComponent } from './pedido-form/pedido-form.component';
import { PedidoListComponent } from './pedido-list/pedido-list.component';
import { routing } from './app.routes';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { PedidosService } from './pedidos.service';
import { PedidoRecebidoComponent } from './pedido-recebido/pedido-recebido.component';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';

@NgModule({
  declarations: [
    AppComponent,
    PedidoFormComponent,
    PedidoListComponent,
    PedidoRecebidoComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    routing,
    AngularFireModule.initializeApp(environment.firebase),
    Ng4LoadingSpinnerModule.forRoot(),
    AngularFirestoreModule
  ],
  providers: [ PedidosService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
