// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  pass: '3079c46d17265a64ce2c44515b76a0f5',
  firebase: {
    apiKey: 'AIzaSyBFNtsXHXwkxZW2kY58O9_f6v7Ep6vbGdE',
    authDomain: 'massa-solidaria.firebaseapp.com',
    databaseURL: 'https://massa-solidaria.firebaseio.com',
    projectId: 'massa-solidaria',
    storageBucket: 'massa-solidaria.appspot.com',
    messagingSenderId: '282946294862'

  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
